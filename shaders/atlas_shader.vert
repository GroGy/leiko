#version 330 core
layout(location = 0) in vec2 l_position;
layout(location = 1) in vec2 l_uv;
layout(location = 2) in float lv_tIndex;

out vec2 vOut_uv;
out float vOut_tIndex;

void main()
{
    gl_Position = vec4(l_position,0.0f,1.0f);
    vOut_uv = l_uv;
    vOut_tIndex = lv_tIndex;
}
