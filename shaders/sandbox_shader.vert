#version 330 core
layout(location = 0) in vec3 l_position;
layout(location = 1) in vec4 l_color;
layout(location = 2) in float l_tiling;

out vec4 vOut_color;
out float vOut_tiling;

void main()
{
    gl_Position = vec4(l_position.x,l_position.y,l_position.z, 1.0f);
    vOut_color = l_color;
    vOut_tiling = l_tiling;
}