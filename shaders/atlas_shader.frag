#version 330 core
layout (location = 0) out vec4 FragColor;
layout (location = 1) out vec4 BrightColor;

in vec2 vOut_uv;
in float vOut_tIndex;

uniform sampler2D u_texture;
uniform sampler2D u_bloomTexture;

uniform bool u_hasBloom[16];

void main() {
    int textureIndex = int(vOut_tIndex);

    vec4 color;
    vec4 bloom;

    color = texture2D(u_texture,vOut_uv);
    if(u_hasBloom[textureIndex]) {
        bloom = texture2D(u_bloomTexture,vOut_uv);
    } else {
        bloom = vec4(0.0,0.0,0.0,1.0);
    }

    BrightColor = bloom;
    FragColor = color;
}
