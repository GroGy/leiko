#version 330 core
out vec4 FragColor;

in vec4 vOut_color;
in float vOut_tiling;

//TODO Change to binary operators
const int TYPE_COLOR = 0;
const int TYPE_TEXTURE = 1;
const int TYPE_TINTED_TEXTURE = 2;

uniform int u_type = 0;
uniform sampler2D u_texture;
uniform vec4 u_tint = vec4(1.0);

void main()
{
    vec4 color = vOut_color;
    /*
    if(u_type == TYPE_COLOR) {
        color = u_tint;
    }
    if(u_type == TYPE_TEXTURE) {
        color = texture(u_texture,vOut_uv);
    }
    */

    FragColor = color;
}