//
// Created by Matty on 14.4.2020.
//

#ifndef LEIKO_ENTRYPOINT_H
#define LEIKO_ENTRYPOINT_H

extern Leiko::Game * Leiko::getGame();

int main(int argc,char** argv) {
    auto app = Leiko::getGame();
    printf("Starting Leiko engine!\n");
    app->start();
    return 0;
}

#endif //LEIKO_ENTRYPOINT_H
