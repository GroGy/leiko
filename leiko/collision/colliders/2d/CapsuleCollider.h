//
// Created by Matty on 14.6.2020.
//

#ifndef LEIKO_CAPSULECOLLIDER_H
#define LEIKO_CAPSULECOLLIDER_H

#include "Collider2D.h"
#include "2DColliders.h"

namespace Leiko {
namespace Collision {
namespace Collision2D {
class CapsuleCollider : public Leiko::Collision::Collision2D::Collider2D {
private:
    glm::vec2 m_a, m_b;
    float m_radius;
public:
    CapsuleCollider(glm::vec2 a, glm::vec2 b, float radius);
    c2Capsule buildC2Capsule();
    bool collides(std::shared_ptr<Collider> other) override;
};
}}}

#endif //LEIKO_CAPSULECOLLIDER_H
