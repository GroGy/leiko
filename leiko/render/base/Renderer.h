//
// Created by Matty on 10.4.2020.
//

#ifndef LEIKO_RENDERER_H
#define LEIKO_RENDERER_H

#include "../types/VertexBuffer.h"
#include "../types/DrawBatch2D.h"
#include <cstdint>

namespace Leiko {
namespace Render {
namespace Types{
    class DrawBatch2D;
}

class Renderer {
private:
    /// FPS limit of renderer
    uint32_t m_pFpsLimit = 60;
    bool m_pActiveWireframe = false;
public:
    uint32_t getFpsLimit() { return m_pFpsLimit; }
    void preRender(glm::vec4 & backgroundColor);
    void drawBuffer(Types::VertexBuffer & vertexBuffer);
    void drawBuffer(Types::DrawBatch2D & drawBatch, Types::VertexLayout layout);
    void switchWireframe();
    static void setLineWidth(uint32_t width);
};
}
}

#endif //LEIKO_RENDERER_H
