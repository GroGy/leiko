//
// Created by Matty on 7.5.2020.
//

#ifndef LEIKO_SHADER_H
#define LEIKO_SHADER_H

#include <memory>
#include <sstream>
#include "../../util/UtilFile.h"
#include "../../lib/glm/glm.hpp"
#include "../../Exceptions.h"
#include "../../Global.h"

namespace Leiko {
namespace Render {
namespace Types {

const int32_t SHADER_INFO_LENGHT = 1024;

class Shader {
private:
    uint32_t m_pShaderProgramId;
public:
    ~Shader();

    friend class ShaderBuilder;

    /// Activates shader program
    void activate();

    /// Set value of integer uniform in shader
    /// \param variableName Name of uniform in shader
    /// \param data Data passed to shader
    void setUniformInt(const std::string& variableName, int32_t data) const;
    /// Set value of boolean uniform in shader
    /// \param variableName Name of uniform in shader
    /// \param data Data passed to shader
    void setUniformBool(const std::string& variableName, bool data) const;
    /// Set value of float uniform in shader
    /// \param variableName Name of uniform in shader
    /// \param data Data passed to shader
    void setUniformFloat(const std::string& variableName, float data) const;
    /// Set value of matrix4 uniform in shader
    /// \param variableName Name of uniform in shader
    /// \param data Data passed to shader
    void setUniformMat4(const std::string& variableName, glm::mat4 data) const;
    /// Set value of vector4 uniform in shader
    /// \param variableName Name of uniform in shader
    /// \param data Data passed to shader
    void setUniformVec4(const std::string& variableName, glm::vec4 data) const;
    /// Set value of vector3 uniform in shader
    /// \param variableName Name of uniform in shader
    /// \param data Data passed to shader
    void setUniformVec3(const std::string& variableName, glm::vec3 data) const;
    /// Set value of vector2 uniform in shader
    /// \param variableName Name of uniform in shader
    /// \param data Data passed to shader
    void setUniformVec2(const std::string& variableName, glm::vec2 data) const;
};

class ShaderBuilder : public std::enable_shared_from_this<ShaderBuilder> {
private:
    int32_t m_pProgramId;

    bool m_pHasGeometryShader = false;
    std::string m_pGeometryShaderPath;
    bool m_pHasTesselationShader = false;
    std::string m_pTesselationShaderPath;

    bool m_pVertexCodeSpecified = false;
    std::string m_pVertexShaderPath;
    bool m_pFragmentCodeSpecified = false;
    std::string m_pFragmentShaderPath;
public:
    /// Shader builder that is used to load shaders, by default you pass path, if you want to pass code directly, you have to call method to switch shader build mode
    /// \param vertex
    /// \param fragment
    ShaderBuilder(std::string vertex, std::string fragment):m_pFragmentShaderPath(std::move(fragment)), m_pVertexShaderPath(std::move(vertex))
    {
    }
    /// Changes mode for vertex shader to be code, not path
    /// \return Builder
    std::shared_ptr<ShaderBuilder> setVertexInputToCode()
    {
        m_pVertexCodeSpecified = true;
        return shared_from_this();
    };
    /// Changes mode for Fragment shader to be code, not path
    /// \return Builder
    std::shared_ptr<ShaderBuilder> setFragmentInputToCode()
    {
        m_pFragmentCodeSpecified = true;
        return shared_from_this();
    };
    /// Enables geometry shader, pass in path (Direct code not yet supported)
    /// \param path Path to geometry shader file
    /// \return Builder
    std::shared_ptr<ShaderBuilder> setGeometryShader(std::string path)
    {
        m_pHasGeometryShader = true;
        m_pGeometryShaderPath = std::move(path);
        return shared_from_this();
    };
    /// Enables tesselation shader, pass in path (Direct code not yet supported)
    /// \param path Path to tesselation shader file
    /// \return Builder
    std::shared_ptr<ShaderBuilder> setTesselationShader(std::string path)
    {
        m_pHasTesselationShader = true;
        m_pTesselationShaderPath = std::move(path);
        return shared_from_this();
    };
    /// Finishes shader build, creates shader
    /// \return Shader
    Shader * finish();
};
}
}
}

#endif //LEIKO_SHADER_H
