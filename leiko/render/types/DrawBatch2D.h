//
// Created by Matty on 7.5.2020.
//

#ifndef LEIKO_DRAWBATCH2D_H
#define LEIKO_DRAWBATCH2D_H

#include <cstdint>
#include <vector>
#include "VertexBuffer.h"
#include "Vertex.h"
#include "Atlas.h"
#include "../base/Renderer.h"

namespace Leiko {
namespace Render {
namespace Types {
class Shader;

enum DrawType : uint32_t {
  POINTS = 0x0000,
  LINES = 0x0001,
  LINE_LOOP = 0x0002,
  LINE_STRIP = 0x0003,
  TRIANGLES = 0x0004,
  TRIANGLE_STRIP = 0x0005,
  TRIANGLE_FAN = 0x0006,
  QUADS = 0x0007
};

/// Drawbatch object used for drawing
class DrawBatch2D {
private:
    std::vector<float> m_pData;
    std::vector<uint32_t> m_pIndexData;
    uint32_t m_pIndicesIndex = 0;
    VertexBuffer m_pVertexBuffer;
    bool m_pHasTexture = false;
    std::shared_ptr<Leiko::Render::Types::Atlas> m_pAtlas;
    DrawType m_pDrawType = TRIANGLES;
public:
    ~DrawBatch2D() = default;
    /// Clears buffers of this draw batch
    void flush();
    /// Finish this buffer, upload its data to GPU
    void finish();
    /// Gets vertex buffer that this batch uses
    /// \return VertexBuffer of this draw batch
    VertexBuffer& getVertexBuffer() { return m_pVertexBuffer; };
    DrawBatch2D(uint32_t bufferPerDraw = 2048, uint32_t indexBufferPerDraw = 3072);
    DrawBatch2D(const Leiko::Render::Types::DrawBatch2D& db);
    void addVertexData(Leiko::Render::Types::Vertex& vertex);
    /// Adds data to draw batch
    /// \param data
    void addData(float data);
    /// Adds data to draw batch
    /// \param data pointer to start of data
    /// \param size float count
    void addData(float* data, uint32_t size);
    /// Adds data to draw batch
    /// \param data
    void addData(std::vector<float>& data);
    /// Adds index data to end of buffer
    /// \param index Index data
    void addIndex(uint32_t index);
    /// Adds index data
    /// \param data Index data
    /// \param size Size of index data
    void addIndex(uint32_t* data, uint32_t size);
    /// Adds index data
    /// \param data Index data
    void addIndex(std::vector<uint32_t>& data);
    /// Sets atlas that this draw batch uses
    /// \param atlas Atlas to use
    void setAtlas(const std::shared_ptr<Leiko::Render::Types::Atlas>& atlas);
    /// Binds atlas that this draw batch uses
    void bindAtlasTextures();
    /// Sets draw type for this draw batch
    /// \param type DrawType to use
    void setDrawType(DrawType type);
    /// Getter for draw type that this draw batch   uses
    /// \return DrawType of this draw batch
    DrawType getDrawType() { return m_pDrawType;}
};
}
}
}
#endif //LEIKO_DRAWBATCH2D_H
