//
// Created by Matty on 16.5.2020.
//

#ifndef LEIKO_ORTHOGRAPHICCAMERA_H
#define LEIKO_ORTHOGRAPHICCAMERA_H

#include "base/Camera.h"

namespace Leiko {
    namespace Render {
        namespace Types {
            class OrthographicCamera : public Leiko::Render::Types::Camera {
            private:
            public:
                std::pair<Leiko::Render::Types::ViewMatrix, Leiko::Render::Types::ProjectionMatrix> getViewMatrix() override;
            };
        }
    }
}

#endif //LEIKO_ORTHOGRAPHICCAMERA_H
