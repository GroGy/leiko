//
// Created by Matty on 10.4.2020.
//

#include "../leiko/glad.h"
#include "../leiko/render/base/Renderer.h"
#include "ErrorCheck.cpp"

void Leiko::Render::Renderer::preRender(glm::vec4 & backgroundColor)
{
    glBindFramebuffer(GL_FRAMEBUFFER,0);
    glClearColor(backgroundColor.r,backgroundColor.g,backgroundColor.b,backgroundColor.a);
    glClear(GL_COLOR_BUFFER_BIT);
    //glCheckError();
}
void Leiko::Render::Renderer::drawBuffer(Types::VertexBuffer & vertexBuffer)
{
    vertexBuffer.bind();
    glDrawElements(GL_TRIANGLES,vertexBuffer.getElementCount(),GL_UNSIGNED_INT,0);
    glBindVertexArray(0);
    //glCheckError();
}

void Leiko::Render::Renderer::drawBuffer(Types::DrawBatch2D & drawBatch, Types::VertexLayout layout){
    if(drawBatch.getVertexBuffer().getElementCount() > 0) {
        drawBatch.getVertexBuffer().bind();
        layout.setAttributePointers();
        glDrawElements(drawBatch.getDrawType(), drawBatch.getVertexBuffer().getElementCount(), GL_UNSIGNED_INT, nullptr);
    }
    //glCheckError();
}
void Leiko::Render::Renderer::switchWireframe()
{
    m_pActiveWireframe = !m_pActiveWireframe;
    if(m_pActiveWireframe) {
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    } else {
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }
}
void Leiko::Render::Renderer::setLineWidth(uint32_t width)
{
    glLineWidth(width);
};