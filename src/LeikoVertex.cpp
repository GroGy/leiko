//
// Created by Matty on 16.5.2020.
//

#include "../leiko/render/primitives/LeikoVertex.h"
uint32_t Leiko::Render::LeikoVertex::getFloatCount()
{
    return 3 + 4 + 2 + 1;
}
Leiko::Render::Types::VertexLayout Leiko::Render::LeikoVertex::getVertexLayout()
{
    auto layout = Types::VertexLayout(3);
    auto posComponent = Types::VertexLayoutComponent(3);
    auto colorComponent = Types::VertexLayoutComponent(4);
    auto uvComponent = Types::VertexLayoutComponent(2);
    auto texturedComponent = Types::VertexLayoutComponent(1);
    layout.addAttributeComponent(posComponent);
    layout.addAttributeComponent(colorComponent);
    layout.addAttributeComponent(uvComponent);
    layout.addAttributeComponent(texturedComponent);
    return layout;
}
Leiko::Render::LeikoVertex::LeikoVertex(glm::vec3 pos, glm::vec4 color, glm::vec2 uv, bool isTextured,glm::vec4 uvDATA)
{
    this->m_pData.reserve(getFloatCount());
    this->m_pData.emplace_back(pos.x);
    this->m_pData.emplace_back(pos.y);
    this->m_pData.emplace_back(pos.z);
    this->m_pData.emplace_back(color.r);
    this->m_pData.emplace_back(color.g);
    this->m_pData.emplace_back(color.b);
    this->m_pData.emplace_back(color.a);
    this->m_pData.emplace_back(uv.x);
    this->m_pData.emplace_back(uv.y);
    this->m_pData.emplace_back(isTextured ? 1.0f : 0.0f);
    //TODO: Add tiling
}
void Leiko::Render::LeikoVertex::operator=(
        const Leiko::Render::LeikoVertex& v)
{
    this->m_pData = v.m_pData;
}
