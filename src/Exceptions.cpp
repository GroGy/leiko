//
// Created by Matty on 10.4.2020.
//

#include "../leiko/Exceptions.h"
Leiko::Exceptions::LeikoException::LeikoException(const std::string& reason, const std::string& source)
{
    m_pMessage = std::string(source + " > " + reason);
}
const char* Leiko::Exceptions::LeikoException::what() const noexcept
{
    return m_pMessage.c_str();
}
