//
// Created by Matty on 25.5.2020.
//

#include "../leiko/render/types/Texture.h"
#include "../leiko/glad.h"
Leiko::Render::Types::TextureRawData::~TextureRawData()
{
    glDeleteTextures(1, &m_id);
}
