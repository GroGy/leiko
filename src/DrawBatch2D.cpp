//
// Created by Matty on 7.5.2020.
//

#include <iostream>
#include <memory>
#include "../leiko/render/types/DrawBatch2D.h"
#include "../leiko/glad.h"
#include "ErrorCheck.cpp"


void Leiko::Render::Types::DrawBatch2D::addData(float* data, uint32_t size)
{

}
void Leiko::Render::Types::DrawBatch2D::addData(std::vector<float>& data)
{
    m_pData.insert(m_pData.end(), data.begin(), data.end());
}
void Leiko::Render::Types::DrawBatch2D::addData(float data)
{
    m_pData.insert(m_pData.end(), data);
}
Leiko::Render::Types::DrawBatch2D::DrawBatch2D(uint32_t bufferPerDraw, uint32_t indexBufferPerDraw) {
    m_pData.reserve(bufferPerDraw);
    m_pIndexData.reserve(indexBufferPerDraw);
}
void Leiko::Render::Types::DrawBatch2D::addIndex(std::vector<uint32_t>& data)
{
    uint32_t low = 100000000;
    uint32_t high = 0;
    for (auto a : data) {
        this->m_pIndexData.emplace_back(a+m_pIndicesIndex); //Leaks
        if (a>high) {
            high = a;
        }
        if (a<low) {
            low = a;
        }
    }
    m_pIndicesIndex += high-low+1;
}
void Leiko::Render::Types::DrawBatch2D::finish()
{
    m_pVertexBuffer.bind();

    this->m_pVertexBuffer.setVertexData(m_pData.size(), m_pData.data(), GL_STATIC_DRAW);
    this->m_pVertexBuffer.setIndexData(m_pIndexData.size(), m_pIndexData.data(), GL_STATIC_DRAW);
    this->m_pVertexBuffer.setElementCount(m_pIndexData.size());
}
void Leiko::Render::Types::DrawBatch2D::addVertexData(Leiko::Render::Types::Vertex& vertex)
{
    this->m_pData.insert(this->m_pData.end(), vertex.data().begin(), vertex.data().end()); // Leaks
}
void Leiko::Render::Types::DrawBatch2D::flush()
{
    this->m_pData.clear();
    this->m_pIndexData.clear();
    this->m_pIndicesIndex = 0;
}
Leiko::Render::Types::DrawBatch2D::DrawBatch2D(const Leiko::Render::Types::DrawBatch2D& db)
{
    this->m_pData = db.m_pData;
    this->m_pIndicesIndex = db.m_pIndicesIndex;
    this->m_pIndexData = db.m_pIndexData;
    this->m_pVertexBuffer = db.m_pVertexBuffer;
}
void Leiko::Render::Types::DrawBatch2D::setAtlas(const std::shared_ptr<Leiko::Render::Types::Atlas>& atlas)
{
    m_pHasTexture = true;
    m_pAtlas = atlas;
}
void Leiko::Render::Types::DrawBatch2D::bindAtlasTextures()
{
    if (m_pHasTexture) {
        m_pAtlas->bind();
    }
    else {
        std::cerr << "WARN: Tried binding texture atlas without it being set\n";
    }
}
void Leiko::Render::Types::DrawBatch2D::addIndex(uint32_t* data, uint32_t size)
{

}
void Leiko::Render::Types::DrawBatch2D::addIndex(uint32_t index)
{

}
void Leiko::Render::Types::DrawBatch2D::setDrawType(Leiko::Render::Types::DrawType type)
{
    m_pDrawType = type;
}
