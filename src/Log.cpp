//
// Created by Matty on 25.5.2020.
//

#include "../leiko/Log.h"

#ifdef LINUX_BUILD
void Leiko::Log::ShowCrashPopup(const std::string& errorMessage)
{
    std::cout << errorMessage << std::endl;
    exit(EXIT_FAILURE);
}
#else
void Leiko::Log::ShowCrashPopup(const std::string& errorMessage)
{
    LPCSTR text = errorMessage.c_str();
    LPCSTR title = "Crashed";

    int msgBoxResult = MessageBox(
            NULL,
            text,
            title,
            MB_ICONERROR | MB_OK
    );

    switch (msgBoxResult) {
    case IDOK:exit(EXIT_FAILURE);
    }
}
#endif
