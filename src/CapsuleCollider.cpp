//
// Created by Matty on 14.6.2020.
//

#include "../leiko/collision/colliders/2d/CapsuleCollider.h"
bool Leiko::Collision::Collision2D::CapsuleCollider::collides(std::shared_ptr<Collider> other)
{
    return false;
}
c2Capsule Leiko::Collision::Collision2D::CapsuleCollider::buildC2Capsule()
{
    auto result = c2Capsule();
    result.a.x = m_a.x;
    result.a.y = m_a.y;
    result.b.x = m_b.x;
    result.b.y = m_b.y;
    result.r = m_radius;
    return result;
}
Leiko::Collision::Collision2D::CapsuleCollider::CapsuleCollider(glm::vec2 a, glm::vec2 b, float radius)
        :Leiko::Collision::Collision2D::Collider2D(CAPSULE_2D), m_a(a), m_b(b), m_radius(radius)
{

}
