//
// Created by mattyvrba on 7/23/20.
//

#include "../leiko/render/types/base/Camera.h"

glm::vec3 Leiko::Render::Types::Camera::getScale() {
    return m_pScale;
}

void Leiko::Render::Types::Camera::setPosition(const glm::vec3 &position) {
    m_pPosition = position;
}

void Leiko::Render::Types::Camera::setRotation(const glm::vec3 &rotation) {
    m_pRotation = rotation;
}

void Leiko::Render::Types::Camera::setScale(const glm::vec3 &scale) {
    m_pScale = scale;
}

glm::vec3 Leiko::Render::Types::Camera::getPosition() {
    return m_pPosition;
}

glm::vec3 Leiko::Render::Types::Camera::getRotation() {
    return m_pRotation;
}
