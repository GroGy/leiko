//
// Created by mattyvrba on 8/4/20.
//

#include "../leiko/util/UtilThread.h"

Leiko::Util::ThreadData::ThreadData(std::string name) : m_pName(std::move(name)), m_pShouldRun(true) {

}

bool Leiko::Util::ThreadData::getShouldRun() {
    return m_pShouldRun;
}

void Leiko::Util::ThreadData::stop() {
    m_pShouldRun = false;
}
