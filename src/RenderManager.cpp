//
// Created by Matty on 16.8.2020.
//

#include "../leiko/render/base/core/RenderManager.h"
void RenderManager::stop()
{
    m_pRenderThreadRun = false;
}
void RenderManager::init()
{

}
void RenderManager::_renderUpdate()
{

}
void RenderManager::_renderInit()
{
    m_pRenderThread = std::make_unique<std::thread>([this]() { this->_renderUpdate(); });
}
