//
// Created by Matty on 15.6.2020.
//

#include "../leiko/render/primitives/2D/EmptyRectangle.h"

void Leiko::Render::Primitives2D::EmptyRectangle::draw(const std::shared_ptr<Types::DrawBatch2D>& batch)
{
    for (auto v : m_pVertexData) {
        batch->addVertexData(v);
    }
    batch->addIndex(m_pIndexData);
    batch->setDrawType(Types::LINES);
}
Leiko::Render::Primitives2D::EmptyRectangle::EmptyRectangle(glm::vec2 pos, glm::vec4 color, glm::vec2 size,
        float rotation) : EmptyRectangle(glm::vec3(pos, 1.0f),color, size, rotation)
{

}
Leiko::Render::Primitives2D::EmptyRectangle::EmptyRectangle(glm::vec3 pos, glm::vec4 color, glm::vec2 size,
        float rotation)
{
    m_pBaseVertexPositions.reserve(4);
    m_pBaseVertexPositions.emplace_back(0.5f, 0.5f, 0.0f, 1.0f);
    m_pBaseVertexPositions.emplace_back(0.5f, -0.5f, 0.0f, 1.0f);
    m_pBaseVertexPositions.emplace_back(-0.5f, 0.5f, 0.0f, 1.0f);
    m_pBaseVertexPositions.emplace_back(-0.5f, -0.5f, 0.0f, 1.0f);
    auto uv = glm::vec4(1.0f);
    m_pVertexData.reserve(4);

    auto transform = glm::rotate(glm::mat4(1.0f), glm::radians(rotation), {0.0f, 0.0f, 1.0f})*glm::scale(glm::mat4(1.0f), {size.x, size.y, 1.0f});

    m_pVertexData.emplace_back(
            LeikoVertex(glm::xyz(glm::vec4(pos, 0.0f)+m_pBaseVertexPositions[0]*transform), color, {uv.z, uv.w},
                    false,uv));
    m_pVertexData.emplace_back(
            LeikoVertex(glm::xyz(glm::vec4(pos, 0.0f)+m_pBaseVertexPositions[1]*transform), color, {uv.z, uv.y},
                    false,uv));
    m_pVertexData.emplace_back(
            LeikoVertex(glm::xyz(glm::vec4(pos, 0.0f)+m_pBaseVertexPositions[2]*transform), color, {uv.x, uv.w},
                    false,uv));
    m_pVertexData.emplace_back(
            LeikoVertex(glm::xyz(glm::vec4(pos, 0.0f)+m_pBaseVertexPositions[3]*transform), color, {uv.x, uv.y},
                    false,uv));

    m_pIndexData.reserve(8);

    m_pIndexData.emplace_back(0);
    m_pIndexData.emplace_back(1);
    m_pIndexData.emplace_back(1);
    m_pIndexData.emplace_back(3);
    m_pIndexData.emplace_back(3);
    m_pIndexData.emplace_back(2);
    m_pIndexData.emplace_back(2);
    m_pIndexData.emplace_back(0);
}
