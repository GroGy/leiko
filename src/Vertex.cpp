//
// Created by Matty on 7.5.2020.
//

#include "../leiko/render/types/Vertex.h"
#include "../leiko/glad.h"
#include "ErrorCheck.cpp"

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wint-to-pointer-cast"

void Leiko::Render::Types::VertexLayoutComponent::setAttributePointer(uint32_t & index, uint32_t & startingOffset,uint32_t totalSize)
{
    glVertexAttribPointer(index, m_pSize, GL_FLOAT, GL_FALSE, totalSize*sizeof(float), (void*)(startingOffset));
    glEnableVertexAttribArray(index++);

    startingOffset += m_pSize*sizeof(float);
    //glCheckError();
}

#pragma GCC diagnostic pop

void Leiko::Render::Types::VertexLayout::setAttributePointers()
{
    uint32_t startingIndex = 0;
    uint32_t startingSize = 0;
    for(auto & a : m_pComponents) {
        a.setAttributePointer(startingIndex,startingSize,m_pTotalSize);
    }
}
void Leiko::Render::Types::VertexLayout::addAttributeComponent(Leiko::Render::Types::VertexLayoutComponent& component)
{
    m_pTotalSize += component.m_pSize;
    m_pComponents.emplace_back(component);
}
