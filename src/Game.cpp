//
// Created by Matty on 10.4.2020.
//

#include <iostream>
#include <chrono>
#ifdef MINGW_WINDOWS_THREAD_LIB
    #include <mingw_threads/mingw.thread.h>
#else
    #include <thread>
#endif
#include "../leiko/render/types/Atlas.h"
#include "../leiko/Game.h"
#include "../leiko/glad.h"
#include "../leiko/Log.h"

void Leiko::Game::internalUpdate()
{

}
void Leiko::Game::internalInit()
{
    Global::s_pGladInitialized = false;
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    Leiko::Render::Types::Atlas::init();
}
void Leiko::Game::start()
{
    try {
        internalInit();
        init();
    } catch (Leiko::Exceptions::LeikoException & e) {
        Leiko::Log::ShowCrashPopup(e.what());
    }

    double frameFrequency = 1.0 / m_pTickRate;

    //Start game loop
    while (!glfwWindowShouldClose(m_pWindow->getGLFWWindow().get())) {
        double startframeTime = glfwGetTime();
        try {
            internalRender();
            render();
            finalizeRender();

            m_pCountedFps++;

            while (m_pElapsedSinceFrame > frameFrequency) {
                update();
                glfwPollEvents();
                m_pElapsedSinceFrame -= frameFrequency;
            }
        }
        catch (Leiko::Exceptions::LeikoException &exception) {
            std::cout << exception.what() << std::endl;
        }

        double endframeTime = glfwGetTime();

        double frameTime = endframeTime - startframeTime; //Frame duration

        m_pElapsedSinceFpsUpdate += frameTime;

        if(frameTime < 1.0 /m_pWindow->getRenderer()->getFpsLimit()) {
            uint32_t sleepDuration = ((1.0 /m_pWindow->getRenderer()->getFpsLimit()) - frameTime)*1000;
            std::this_thread::sleep_for(std::chrono::milliseconds(sleepDuration));
        }

        double endframeTimeAfterWait = glfwGetTime();
        double frameTimeAfterWait = endframeTimeAfterWait - startframeTime;

        if(m_pElapsedSinceFpsUpdate > 1.0) { //Time since last fps count is longer than one second
            m_pFps = m_pCountedFps;
            m_pCountedFps = 0;
            m_pElapsedSinceFpsUpdate = 0.0;
        }

        m_pElapsedSinceFrame += frameTimeAfterWait;
    }

    Leiko::Render::Types::Atlas::deinit();
}
void Leiko::Game::internalRender()
{
    if(m_pWindow != nullptr) {
        m_pWindow->preRender();
    }
}
void Leiko::Game::finalizeRender()
{
    if(m_pWindow != nullptr) {
        m_pWindow->finalizeRender();
    }
}
