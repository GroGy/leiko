//
// Created by Matty on 25.5.2020.
//

#include <algorithm>
#include "../leiko/render/types/Atlas.h"
#include "../leiko/render/types/DrawBatch2D.h"
#include "../leiko/render/Window.h"
#include "../leiko/glad.h"
#define STB_IMAGE_IMPLEMENTATION
#include "../leiko/lib/stb_image.h"

std::shared_ptr<Leiko::Render::Types::Texture> Leiko::Render::Types::Atlas::loadTextureFromVram(uint32_t id,
        uint32_t width, uint32_t height)
{
    auto texture = std::make_shared<Leiko::Render::Types::Texture>();

    texture->m_atlas = this;

    m_pTextures.emplace_back(
            std::pair<std::shared_ptr<Leiko::Render::Types::TextureRawData>,
                      std::shared_ptr<Leiko::Render::Types::Texture>>(
                    std::make_shared<Leiko::Render::Types::TextureRawData>(id, width, height),
                    texture));

    return texture;
}
std::shared_ptr<Leiko::Render::Types::Texture> Leiko::Render::Types::Atlas::loadTexture(const std::string& path)
{
    auto texture = std::make_shared<Leiko::Render::Types::Texture>();

    texture->m_atlas = this;

    uint32_t textureID;
    int32_t textureWidth, textureHeight;

    if (!Leiko::Util::UtilFile::fileExists(path)) {
        throw Leiko::Exceptions::LeikoException("Texture not found > "+path, "Texture Loading");
    }

    stbi_set_flip_vertically_on_load(true);
    int nrChannels = 4;
    glGenTextures(1, &textureID);
    glBindTexture(GL_TEXTURE_2D, textureID);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    uint8_t* data = stbi_load(path.c_str(), &textureWidth, &textureHeight, &nrChannels, STBI_rgb_alpha);
    if (data) {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, textureWidth, textureHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);
    }
    else {
        throw Leiko::Exceptions::LeikoException("Texture not found > "+path, "Texture Loading");
    }
    stbi_image_free(data);

    m_pTextures.emplace_back(
            std::pair<std::shared_ptr<Leiko::Render::Types::TextureRawData>,
                      std::shared_ptr<Leiko::Render::Types::Texture>>(
                    std::make_shared<Leiko::Render::Types::TextureRawData>(textureID, textureWidth, textureHeight),
                    texture));

    return texture;
}
void Leiko::Render::Types::Atlas::_loadAtlasShader()
{
    auto vertexCode = "#version 330 core\n"
                      "layout(location = 0) in vec2 l_position;\n"
                      "layout(location = 1) in vec2 l_uv;\n"
                      "out vec2 vOut_uv;\n"
                      "void main()\n"
                      "{\n"
                      "    gl_Position = vec4(l_position,0.0f,1.0f);\n"
                      "    vOut_uv = l_uv;\n"
                      "}\0";

    auto fragmentCode = "#version 330 core\n"
                        "layout (location = 0) out vec4 FragColor;\n"
                        "layout (location = 1) out vec4 BrightColor;\n"
                        "in vec2 vOut_uv;\n"
                        "uniform sampler2D u_texture;\n"
                        "uniform sampler2D u_bloomTexture;\n"
                        "uniform bool u_hasBloom;\n"
                        "void main() {\n"
                        "  vec4 color;\n"
                        "  vec4 bloom;\n"
                        "  color = texture(u_texture,vOut_uv);\n"
                        "  float brightness = dot(color.rgb, vec3(0.2126, 0.7152, 0.0722));\n"
                        "  if(brightness > 1.0) {\n"
                        "      bloom = vec4(color.rgb,1.0);\n"
                        "  } else {\n"
                        "     bloom = vec4(0.0,0.0,0.0,1.0);\n"
                        "  }\n"

                        "  BrightColor = bloom;\n"
                        "  FragColor = color;\n"
                        " }\0";

    auto shaderBuilder = std::make_shared<Leiko::Render::Types::ShaderBuilder>(vertexCode, fragmentCode)
            ->setFragmentInputToCode()->setVertexInputToCode();

    s_atlasShader = shaderBuilder->finish();
    s_isAtlasShaderLoaded = true;
}
Leiko::Render::Types::Atlas::Atlas(uint32_t sideSize)
        :Atlas(sideSize, sideSize)
{

}
void Leiko::Render::Types::Atlas::init()
{
    s_atlasShader = nullptr;
    s_isAtlasShaderLoaded = false;
}
void Leiko::Render::Types::Atlas::deinit()
{
    delete s_atlasShader;
    s_isAtlasShaderLoaded = false;
}
void Leiko::Render::Types::Atlas::finish()
{
    glBindFramebuffer(GL_FRAMEBUFFER, m_pFbo);
    uint32_t attachmentsUi[2] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1};

    glDrawBuffers(2, attachmentsUi);
    glBindTexture(GL_TEXTURE_2D, m_pTextureId);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, m_pWidth, m_pHeight, 0, GL_RGBA,
            GL_UNSIGNED_BYTE, nullptr);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_pTextureId, 0);

    glBindTexture(GL_TEXTURE_2D, m_pBloomTextureId);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, m_pWidth, m_pHeight, 0, GL_RGBA,
            GL_UNSIGNED_BYTE, nullptr);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, m_pBloomTextureId, 0);

    glBindRenderbuffer(GL_RENDERBUFFER, m_pRbo);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, m_pWidth, m_pHeight);

    std::vector<AtlasVertex> vertices;
    std::vector<uint32_t> indices = {0, 1, 3, 0, 3, 2};

    vertices.reserve(4);

    s_atlasShader->activate();
    glClearColor(0.0f, 0.5f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    glViewport(0, 0, m_pWidth, m_pHeight);
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    if (m_pTextures.size()>1) {
        std::sort(m_pTextures.begin(), m_pTextures.end(),
                [](const std::pair<std::shared_ptr<Leiko::Render::Types::TextureRawData>,
                                   std::shared_ptr<Leiko::Render::Types::Texture>>& a,
                        const std::pair<std::shared_ptr<Leiko::Render::Types::TextureRawData>,
                                        std::shared_ptr<Leiko::Render::Types::Texture>>& b) -> bool {
                  return a.first->m_height>b.first->m_height;
                });
    }

    std::vector<AtlasFreeSpace> spaces;
    spaces.emplace_back(AtlasFreeSpace(m_pWidth, m_pHeight, 0, 0));
    auto batch = Leiko::Render::Types::DrawBatch2D();

    for (auto& t : m_pTextures) {
        batch.flush();
        vertices.clear();
        s_atlasShader->setUniformBool("u_hasBloom", false);
        s_atlasShader->setUniformInt("u_texture", 0);
        glm::vec4 uv;

        bool dontFit = true;
        for (auto i = (int32_t) spaces.size()-1; i>=0; i--) {
            if (t.first->m_height>spaces.at(i).m_height ||
                    t.first->m_width>spaces.at(i).m_width) {
                continue;
            }

            //Sprite fits
            uv = glm::vec4(
                    spaces.at(i).m_x/m_pWidth,
                    spaces.at(i).m_y/m_pHeight,
                    spaces.at(i).m_x/m_pWidth+(float) t.first->m_width/m_pWidth,
                    spaces.at(i).m_y/m_pHeight+(float) t.first->m_height/m_pHeight
            );

            //Exact size
            if (t.first->m_height==spaces.at(i).m_height &&
                    t.first->m_width==spaces.at(i).m_width) {
                spaces.erase(spaces.begin()+i);
            }
            else if (t.first->m_height==spaces.at(i).m_height) {
                spaces.at(i).m_x += t.first->m_width;
                spaces.at(i).m_width -= t.first->m_width;
            }
            else if (t.first->m_width==spaces.at(i).m_width) {
                spaces.at(i).m_y += t.first->m_height;
                spaces.at(i).m_height -= t.first->m_height;
            }
            else {
                //Smaller
                spaces.emplace_back(AtlasFreeSpace(
                        spaces.at(i).m_width-t.first->m_width,
                        t.first->m_height,
                        spaces.at(i).m_x+t.first->m_width,
                        spaces.at(i).m_y
                ));
                spaces.at(i).m_y += t.first->m_height;
                spaces.at(i).m_height -= t.first->m_height;
            }
            dontFit = false;
            break;
        }
        if (dontFit) {
            throw Leiko::Exceptions::LeikoException("Sprite does not fit atlas", "Atlas Baking");
        }

        vertices.emplace_back(AtlasVertex({-1.0f+2*uv.x, -1.0f+2*uv.y}, {0.0f, 0.0f}));
        vertices.emplace_back(AtlasVertex({-1.0f+2*uv.z, -1.0f+2*uv.y}, {1.0f, 0.0f}));
        vertices.emplace_back(AtlasVertex({-1.0f+2*uv.x, -1.0f+2*uv.w}, {0.0f, 1.0f}));
        vertices.emplace_back(AtlasVertex({-1.0f+2*uv.z, -1.0f+2*uv.w}, {1.0f, 1.0f}));

        for (auto& v : vertices) {
            batch.addVertexData(v);
        }
        batch.addIndex(indices);

        batch.finish();

        batch.getVertexBuffer().bind();

        vertices.begin()->getVertexLayout().setAttributePointers();

        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, t.first->m_id);
        glDrawElements(GL_TRIANGLES, batch.getVertexBuffer().getElementCount(), GL_UNSIGNED_INT, nullptr);

        t.second->m_pAtlasUv = uv;
    }

    m_pTextures.clear();
    glBindTexture(GL_TEXTURE_2D, 0);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glViewport(0, 0, Leiko::Render::Window::GetWindowWidth(), Leiko::Render::Window::GetWindowHeight());
}
Leiko::Render::Types::Atlas::~Atlas()
{
    glDeleteFramebuffers(1, &m_pFbo);
    glDeleteTextures(1, &m_pTextureId);
    glDeleteTextures(1, &m_pBloomTextureId);
    glDeleteRenderbuffers(1, &m_pRbo);
    glDeleteBuffers(1, &m_pVbo);
    glDeleteBuffers(1, &m_pEbo);
    glDeleteVertexArrays(1, &m_pVao);
}
Leiko::Render::Types::Atlas::Atlas(uint32_t width, uint32_t height)
        :m_pWidth(width), m_pHeight(height)
{
    m_pTextures.reserve(128); //TODO Add to constructor default paramater

    if (!s_isAtlasShaderLoaded) {
        _loadAtlasShader();
    }

    glGenFramebuffers(1, &m_pFbo);
    glGenTextures(1, &m_pTextureId);
    glGenTextures(1, &m_pBloomTextureId);
    glGenRenderbuffers(1, &m_pRbo);
    glGenBuffers(1, &m_pVbo);
    glGenBuffers(1, &m_pEbo);
    glGenVertexArrays(1, &m_pVao);
}
Leiko::Render::Types::Shader* Leiko::Render::Types::Atlas::s_atlasShader;
bool Leiko::Render::Types::Atlas::s_isAtlasShaderLoaded;
uint32_t Leiko::Render::Types::AtlasVertex::getFloatCount()
{
    return 4;
}
Leiko::Render::Types::VertexLayout Leiko::Render::Types::AtlasVertex::getVertexLayout()
{
    auto layout = Types::VertexLayout(2);
    auto posComponent = Types::VertexLayoutComponent(2);
    auto uvComponent = Types::VertexLayoutComponent(2);
    layout.addAttributeComponent(posComponent);
    layout.addAttributeComponent(uvComponent);
    return layout;
}
Leiko::Render::Types::AtlasVertex::AtlasVertex(glm::vec2 pos, glm::vec2 uv)
{
    this->m_pData.reserve(getFloatCount());
    this->m_pData.emplace_back(pos.x);
    this->m_pData.emplace_back(pos.y);
    this->m_pData.emplace_back(uv.x);
    this->m_pData.emplace_back(uv.y);
}
void Leiko::Render::Types::Atlas::bind()
{
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, m_pTextureId);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, m_pBloomTextureId);
}
