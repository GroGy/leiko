//
// Created by Matty on 14.6.2020.
//

#include "../leiko/collision/colliders/2d/AABBCollider.h"
bool Leiko::Collision::Collision2D::AABBCollider::collides(std::shared_ptr<Collider> other)
{
    return false;
}
c2AABB Leiko::Collision::Collision2D::AABBCollider::buildC2AABB()
{
    auto result = c2AABB();
    result.min.x = m_pPos.x + m_pMin.x;
    result.min.y = m_pPos.y + m_pMin.y;
    result.max.x = m_pPos.x + m_pMax.x;
    result.max.y = m_pPos.y + m_pMax.y;
    return result;
}
Leiko::Collision::Collision2D::AABBCollider::AABBCollider(const  glm::vec2 &min,const  glm::vec2 &max,const glm::vec2 &pos)
        :Leiko::Collision::Collision2D::Collider2D(AABB_2D), m_pMin(min), m_pMax(max),m_pPos(pos)
{

}
void Leiko::Collision::Collision2D::AABBCollider::setPosition(const glm::vec2& pos)
{
    m_pPos = pos;
}
void Leiko::Collision::Collision2D::AABBCollider::setSize(const glm::vec2& min, const glm::vec2& max)
{
    this->m_pMin = min;
    this->m_pMax = max;
}
glm::vec2 Leiko::Collision::Collision2D::AABBCollider::getPosition()
{
    return m_pPos;
}
