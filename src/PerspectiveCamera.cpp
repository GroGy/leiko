//
// Created by mattyvrba on 7/27/20.
//

#include "../leiko/render/types/PerspectiveCamera.h"
#include "../leiko/lib/glm/glm.hpp"
#include "../leiko/lib/glm/gtc/matrix_transform.hpp"

std::pair<Leiko::Render::Types::ViewMatrix, Leiko::Render::Types::ProjectionMatrix> Leiko::Render::Types::PerspectiveCamera::getViewMatrix() {

    auto view = glm::lookAt(m_pPosition,Leiko::Util::UtilMath::getForwardFromRotation(m_pRotation),glm::vec3(0,1,0));
    auto projectionBase = glm::perspective(90.0f,16.0f/9.0f,m_pNear,m_pFar);
            return std::pair<ViewMatrix, ProjectionMatrix>(view, projectionBase);
}
