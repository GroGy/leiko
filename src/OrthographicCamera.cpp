//
// Created by Matty on 16.5.2020.
//

#include "../leiko/render/types/OrthographicCamera.h"
#include "../leiko/lib/glm/gtc/matrix_transform.hpp"
#include "../leiko/util/UtilMath.h"

std::pair<Leiko::Render::Types::ViewMatrix, Leiko::Render::Types::ProjectionMatrix>
Leiko::Render::Types::OrthographicCamera::getViewMatrix()  {
    auto view = glm::lookAt(m_pPosition,Leiko::Util::UtilMath::getForwardFromRotation(m_pRotation),glm::vec3(0,1,0));
    // Sides select how big space is squished to view space, aka -1 to 1 on both axes, if Left is 0 and right is 800, space between 0 and 800 will be mapped to -1 - 1
    /*
    auto projectionBase = glm::ortho(glm::vec3(-1.0f, 0.0f, 0.0f), glm::vec3(1.0f, 0.0f, 0.0f),
                                     glm::vec3(0.0f, -1.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
    */
    auto projectionResult = glm::mat4();

    return std::pair<ViewMatrix, ProjectionMatrix>(view, projectionResult);
}
