//
// Created by Matty on 7.5.2020.
//

#include "../leiko/render/types/VertexBuffer.h"
#include "../leiko/glad.h"
#include "ErrorCheck.cpp"

void Leiko::Render::Types::VertexBuffer::bind()
{
    glBindVertexArray(m_pGlVao);
    glBindBuffer(GL_ARRAY_BUFFER, m_pGlVbo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_pGlEbo);
    //glCheckError();
}
Leiko::Render::Types::VertexBuffer::VertexBuffer()
{
    glGenVertexArrays(1,&m_pGlVao);
    glGenBuffers(1,&m_pGlVbo);
    glGenBuffers(1,&m_pGlEbo);
    //glCheckError();
}
Leiko::Render::Types::VertexBuffer::~VertexBuffer()
{
    glDeleteVertexArrays(1,&m_pGlVao);
    glDeleteBuffers(1,&m_pGlVbo);
    glDeleteBuffers(1,&m_pGlEbo);
    //glCheckError();
}
void Leiko::Render::Types::VertexBuffer::setVertexData(uint32_t size, float* data, int32_t drawType)
{
    glBindVertexArray(m_pGlVao);
    glBindBuffer(GL_ARRAY_BUFFER, m_pGlVbo);
    glBufferData(GL_ARRAY_BUFFER, size*sizeof(float), data,drawType);
    glBindVertexArray(0);
    //glCheckError();
}
void Leiko::Render::Types::VertexBuffer::setIndexData(uint32_t size, uint32_t* data, int32_t drawType)
{
    glBindVertexArray(m_pGlVao);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_pGlEbo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, size*sizeof(uint32_t), data,drawType);
    glBindVertexArray(0);
    //glCheckError();
}

