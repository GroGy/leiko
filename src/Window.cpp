//
// Created by Matty on 10.4.2020.
//

#include <iostream>
#include "../leiko/render/Window.h"
#include "../leiko/Leiko.h"
#include "../leiko/glad.h"
const std::string& Leiko::Render::Window::getWindowName()
{
    return m_pWindowName;
}
std::unique_ptr<GLFWwindow,Leiko::Render::GLFWWindowDestructor> & Leiko::Render::Window::getGLFWWindow()
{
    return m_pGLFWWindow;
}
void Leiko::Render::Window::preRender()
{
    m_pRenderer->preRender(m_pBackgroundColor);
}
void Leiko::Render::Window::finalizeRender()
{
    glfwSwapBuffers(m_pGLFWWindow.get());
}

Leiko::Render::WindowBuilder::WindowBuilder(uint32_t width,uint32_t height,std::unique_ptr<Renderer> renderer) : m_pWidth(width), m_pHeight(height), m_pRenderer(std::move(renderer))
{

}
Leiko::Render::Window* Leiko::Render::WindowBuilder::finish()
{
    if(!m_pHasColor) {
        throw Leiko::Exceptions::LeikoException("Background color is not set!","Window Creation");
    }

    if(!m_pHasName) {
        throw Leiko::Exceptions::LeikoException("Window name is not set!","Window Creation");
    }

    auto finished = new Leiko::Render::Window();
    finished->m_pBackgroundColor = m_pBackgroundColor;
    finished->m_pWindowWidth = m_pWidth;
    finished->m_pWindowHeight = m_pHeight;
    finished->m_pWindowName = m_pWindowName;
    finished->m_pRenderer = std::move(m_pRenderer);

    GLFWwindow* window = glfwCreateWindow((int32_t)finished->m_pWindowWidth,(int32_t)finished->m_pWindowHeight, finished->m_pWindowName.c_str(), NULL, NULL);
    if (window == nullptr)
    {
        glfwTerminate();
        throw Leiko::Exceptions::LeikoException("Failed to initialize GLFW Window","Window Creation");
    }
    glfwMakeContextCurrent(window);
    glfwSetWindowUserPointer(window,finished);
    glfwSetFramebufferSizeCallback(window, [](GLFWwindow* window, int32_t width, int32_t height){
        auto& self = *static_cast<Window*>(glfwGetWindowUserPointer(window));
        self.FBSizeCallback(window,width,height);
    });

    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        throw Leiko::Exceptions::LeikoException("Failed to initialize GLAD","App initialization");
    } else {
        Leiko::Global::s_pGladInitialized = true;
    }

    finished->m_pGLFWWindow = std::unique_ptr<GLFWwindow,Leiko::Render::GLFWWindowDestructor>(window);
    return finished;
}
std::shared_ptr<Leiko::Render::WindowBuilder> Leiko::Render::WindowBuilder::setBackgroundColor(const glm::vec4 & bgColor)
{
    this->m_pBackgroundColor = bgColor;
    this->m_pHasColor = true;
    return shared_from_this();
}
std::shared_ptr<Leiko::Render::WindowBuilder> Leiko::Render::WindowBuilder::setWindowName(const std::string & name) {
    this->m_pHasName = true;
    this->m_pWindowName = name;
    return shared_from_this();
};

void  Leiko::Render::Window::FBSizeCallback(GLFWwindow* window, int32_t width, int32_t height)
{
    glViewport(0, 0, width, height);
}

uint32_t Leiko::Render::Window::m_pWindowWidth;
uint32_t Leiko::Render::Window::m_pWindowHeight;
std::unique_ptr<Leiko::Render::Renderer> Leiko::Render::Window::m_pRenderer;