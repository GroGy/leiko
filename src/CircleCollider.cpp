//
// Created by Matty on 14.6.2020.
//

#include <iostream>
#include "../leiko/collision/colliders/2d/CircleCollider.h"

bool Leiko::Collision::Collision2D::CircleCollider::collides(std::shared_ptr<Collider> other)
{
    if (auto col2D = std::dynamic_pointer_cast<Collider2D>(other)) {
        switch (col2D->getType()) {
        case CIRCLE_2D: {
            auto otherCircle = std::dynamic_pointer_cast<CircleCollider>(other);
            return c2CircletoCircle(this->buildC2Circle(), otherCircle->buildC2Circle());
        }
        case CAPSULE_2D: {
            auto otherCapsule = std::dynamic_pointer_cast<CapsuleCollider>(other);
            return c2CircletoCapsule(this->buildC2Circle(), otherCapsule->buildC2Capsule());
        }
        case AABB_2D: {
            auto otherAABB = std::dynamic_pointer_cast<AABBCollider>(other);
            auto result = c2CircletoAABB(this->buildC2Circle(), otherAABB->buildC2AABB());
            return result;
        }
        case RAY_2D: {
            throw Leiko::Exceptions::LeikoException(
                    "You can't check if ray collides from object to ray, only other way around", "COLLISION CHECK");
        }
        }
    } else {
        throw Leiko::Exceptions::LeikoException("Can't detect collision between 2D and 3D Collider","COLLISION CHECK");
    }
    return false;
}
Leiko::Collision::Collision2D::CircleCollider::CircleCollider(const glm::vec2 & pos, float radius)
        :Leiko::Collision::Collision2D::Collider2D(CIRCLE_2D), m_pPos(pos), m_pRadius(radius)
{

}
c2Circle Leiko::Collision::Collision2D::CircleCollider::buildC2Circle()
{
    auto result = c2Circle();
    result.p.x = m_pPos.x;
    result.p.y = m_pPos.y;
    result.r = m_pRadius;
    return result;
}
void Leiko::Collision::Collision2D::CircleCollider::setPosition(const glm::vec2& pos)
{
    m_pPos = pos;
}
void Leiko::Collision::Collision2D::CircleCollider::setRadius(float radius)
{
    m_pRadius = radius;
}
