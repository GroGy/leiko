//
// Created by Matty on 7.5.2020.
//

#include <iostream>
#include "../leiko/render/primitives/2D/Quad.h"
void Leiko::Render::Primitives2D::Quad::draw(const std::shared_ptr<Types::DrawBatch2D>& batch)
{
    for (auto v : m_pVertexData) {
        batch->addVertexData(v);
    }
    batch->addIndex(m_pIndexData);
    batch->setDrawType(Types::TRIANGLES);
}
Leiko::Render::Primitives2D::Quad::Quad(glm::vec2 pos, glm::vec2 size, float rotation,
        const std::shared_ptr<Types::Texture>& texture, const glm::vec2& tilingFactor)
        :Quad(pos, glm::vec4(1.0), size, rotation, texture, tilingFactor)
{

}
Leiko::Render::Primitives2D::Quad::Quad(glm::vec3 pos, glm::vec4 color, glm::vec2 size, float rotation,
        const std::shared_ptr<Types::Texture>& texture, const glm::vec2& tilingFactor)
{
    m_pBaseVertexPositions.reserve(4);
    m_pBaseVertexPositions.emplace_back(0.5f, 0.5f, 0.0f, 1.0f);
    m_pBaseVertexPositions.emplace_back(0.5f, -0.5f, 0.0f, 1.0f);
    m_pBaseVertexPositions.emplace_back(-0.5f, 0.5f, 0.0f, 1.0f);
    m_pBaseVertexPositions.emplace_back(-0.5f, -0.5f, 0.0f, 1.0f);

    glm::vec4 uv = texture==nullptr ? glm::vec4(1.0f) : texture->getUv();
    m_pVertexData.reserve(4);

    auto transform = glm::rotate(glm::mat4(1.0f), glm::radians(rotation), {0.0f, 0.0f, 1.0f})*glm::scale(glm::mat4(1.0f), {size.x, size.y, 1.0f});

    m_pVertexData.emplace_back(
            LeikoVertex(glm::xyz(glm::vec4(pos, 0.0f)+m_pBaseVertexPositions[0]*transform), color, {uv.z, uv.w},
                    texture!=nullptr,uv));
    m_pVertexData.emplace_back(
            LeikoVertex(glm::xyz(glm::vec4(pos, 0.0f)+m_pBaseVertexPositions[1]*transform), color, {uv.z, uv.y},
                    texture!=nullptr,uv));
    m_pVertexData.emplace_back(
            LeikoVertex(glm::xyz(glm::vec4(pos, 0.0f)+m_pBaseVertexPositions[2]*transform), color, {uv.x, uv.w},
                    texture!=nullptr,uv));
    m_pVertexData.emplace_back(
            LeikoVertex(glm::xyz(glm::vec4(pos, 0.0f)+m_pBaseVertexPositions[3]*transform), color, {uv.x, uv.y},
                    texture!=nullptr,uv));

    m_pIndexData.reserve(6);

    m_pIndexData.emplace_back(0);
    m_pIndexData.emplace_back(1);
    m_pIndexData.emplace_back(3);

    m_pIndexData.emplace_back(0);
    m_pIndexData.emplace_back(3);
    m_pIndexData.emplace_back(2);
}

Leiko::Render::Primitives2D::Quad::Quad(glm::vec2 pos, glm::vec4 color, glm::vec2 size, float rotation,
        const std::shared_ptr<Types::Texture>& texture, const glm::vec2& tilingFactor)
        :Quad(glm::vec3(pos.x, pos.y, 0.0f), color, size, rotation, texture, tilingFactor)
{
}
